#!/usr/bin/env python

"""
Constants for the PyOWM library
"""

PYOWM_VERSION = '1.2.0'
LATEST_OWM_API_VERSION = '2.5'
